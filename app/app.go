package app

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/app/actions/analyze"
	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/app/commands"
)

type App struct {
	cli      *cli.App
	register *commands.Register
}

func (app *App) Run(args []string) error {
	app.cli.Commands = app.register.GetCommands()

	if len(app.cli.Commands) < 1 {
		logrus.Fatal("no commands registered")
	}

	return app.cli.Run(args)
}

func (app *App) RegisterCommands() {
	analyze.RegisterAnalyzeCommand(app.register)
}

func NewApp() *App {
	cli := cli.NewApp()
	cli.Name = "gitlab-structlog"
	cli.Usage = "Analyze GitLab structured logs"
	cli.UsageText = "gitlab-structlog command [subcommand] [arguments...]"
	cli.Version = "v0.1a"

	return &App{cli: cli, register: commands.NewRegister()}
}
