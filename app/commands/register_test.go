package commands

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockAction struct {
	mock.Mock
}

func (action *MockAction) Execute(context *Context) error {
	args := action.Called(context)

	return errors.New(args.String(0))
}

func TestGetCommands(t *testing.T) {
	register := NewRegister()
	command := Command{
		Registrable: Registrable{
			Path:   "my-command",
			Config: Config{Name: "my-command"}},
		Handler: new(MockAction),
	}
	register.RegisterCommand(command)

	commands := register.GetCommands()

	assert.Equal(t, 1, len(commands))
}
