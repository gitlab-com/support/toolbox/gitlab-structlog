package commands

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var contextCtx context.Context

var defaultContext = func() context.Context {
	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGTERM, syscall.SIGQUIT, os.Interrupt)

	ctx, cancelFunc := context.WithCancel(context.Background())
	go func() {
		sig := <-signals
		logrus.Warnf("Received %q signal; quitting", sig)
		cancelFunc()
	}()

	return ctx
}

// CLI action function invocation context
type Context struct {
	Cli *cli.Context
	Ctx context.Context
}

func newContext(cliCtx *cli.Context) *Context {
	if contextCtx == nil {
		contextCtx = defaultContext()
	}

	return &Context{
		Cli: cliCtx,
		Ctx: contextCtx,
	}
}
