package app

import (
	"testing"

	"github.com/stretchr/testify/mock"

	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/app/commands"
)

type MockAction struct {
	mock.Mock
}

func (action *MockAction) Execute(context *commands.Context) error {
	args := action.Called(context)

	return args.Error(0)
}

func TestAppRun(t *testing.T) {
	app := NewApp()

	action := new(MockAction)
	action.On("Execute", mock.Anything).Return(nil)

	category := commands.Category{
		Registrable: commands.Registrable{
			Path:   "my-category",
			Config: commands.Config{Name: "my-category"},
		},
	}

	command := commands.Command{
		Registrable: commands.Registrable{
			Path:   "my-category/my-command",
			Config: commands.Config{Name: "my-command"},
		},
		Handler: action,
	}

	app.register.RegisterCategory(category)
	app.register.RegisterCommand(command)

	app.Run([]string{"gitlab-structlog", "my-category", "my-command"})

	action.AssertNumberOfCalls(t, "Execute", 1)
}
