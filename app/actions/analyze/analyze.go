package analyze

import (
	"errors"

	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/app/commands"
	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/graph"
	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/logs"
)

type analyzeAction struct {
	File       string `short:"f" long:"file" env:"GITLAB_ANALYZE_LOG_FILE" description:"Log file path"`
	Controller string `short:"c" long:"controller" env:"GITLAB_ANALYZE_CONTROLLER" description:"Controller filter"`
	Graph      string `short:"g" long:"graph" env:"GITLAB_ANALYZE_GRAPH" description:"Duration series graph output file"`
}

func (action *analyzeAction) Execute(context *commands.Context) error {
	if len(action.File) == 0 || len(action.Controller) == 0 {
		return errors.New("You need to specify --controller and --file!")
	}

	stats := logs.LoadRails(action.File, action.Controller)
	stats.Print()

	if len(action.Graph) > 0 {
		graph := graph.NewDurationSeries(stats)
		graph.ToFile(action.Graph)
	}

	return nil
}

func newAnalyzeCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "analyze",
			Config: commands.Config{
				Name:        "analyze",
				Aliases:     []string{},
				Usage:       "Analyze logs",
				Description: "Analyze performance based on structured logs",
			},
		},
		Handler: new(analyzeAction),
	}
}

func RegisterAnalyzeCommand(register *commands.Register) {
	register.RegisterCommand(newAnalyzeCommand())
}
