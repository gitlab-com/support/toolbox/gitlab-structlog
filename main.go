package main

import (
	"os"

	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/app"
)

func main() {
	app := app.NewApp()
	app.RegisterCommands()
	err := app.Run(os.Args)

	if err != nil {
		panic(err)
	}
}
