package graph

import (
	"fmt"
	"os"
	"time"

	"github.com/wcharczuk/go-chart"
)

type Duration struct {
	xTime     []time.Time
	yDuration []float64
}

type DurationSeries interface {
	ToDurationSeries() ([]time.Time, []float64)
}

func (series *Duration) ToFile(output string) {
	fmt.Printf("Generating timeseries graph ...")

	graph := chart.Chart{
		XAxis: chart.XAxis{
			Style: chart.StyleShow(),
		},
		YAxis: chart.YAxis{
			Style: chart.StyleShow(),
		},
		Series: []chart.Series{
			chart.TimeSeries{
				XValues: series.xTime,
				YValues: series.yDuration,
			},
		},
	}

	file, err := os.Create(output)
	if err != nil {
		panic("can not create output graph file")
	}
	defer file.Close()

	graph.Render(chart.PNG, file)
	fmt.Printf(" %s\n", output)
}

func NewDurationSeries(series DurationSeries) *Duration {
	xTime, yDuration := series.ToDurationSeries()

	return &Duration{xTime: xTime, yDuration: yDuration}
}
