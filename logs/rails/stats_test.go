package rails

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func newStats() *Stats {
	var entries Entries

	entries = append(entries, Entry{Duration: 100})
	entries = append(entries, Entry{Duration: 150})
	entries = append(entries, Entry{Duration: 250})
	entries = append(entries, Entry{Duration: 100})

	return NewStats(entries)
}

func TestProcess(t *testing.T) {
	stats := newStats()

	assert.Equal(t, stats.size, 4)
	assert.Equal(t, stats.avg_duration, float64(150))
	assert.Equal(t, stats.min_duration, float64(100))
	assert.Equal(t, stats.max_duration, float64(250))
}

func TestPrint(t *testing.T) {
	stats := newStats()
	output := new(bytes.Buffer)

	stats.Print(output)

	assert.Contains(t,
		output.String(),
		"Log entries: 4, avg: 150.00, min: 100.00, max: 250.00 [ms]",
	)
}
