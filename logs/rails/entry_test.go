package rails

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestEntryMatches(t *testing.T) {
	entry := Entry{
		Controller: "MyController",
		Action:     "my_action",
		Format:     "json",
	}

	endpoint := Endpoint{Path: "MyController#my_action.json"}
	assert.True(t, entry.Matches(endpoint.ToFilter()))

	endpoint = Endpoint{Path: "MyController#my_action.html"}
	assert.False(t, entry.Matches(endpoint.ToFilter()))

	endpoint = Endpoint{Path: "MyController#myaction.json"}
	assert.False(t, entry.Matches(endpoint.ToFilter()))

	endpoint = Endpoint{Path: "Controller#my_action.json"}
	assert.False(t, entry.Matches(endpoint.ToFilter()))
}

func TestEntriesToDurationSeries(t *testing.T) {
	var entries Entries

	today := time.Now()
	yesterday := time.Now().Add(-24 * time.Hour)

	entries = append(entries, Entry{Time: today, Duration: 100})
	entries = append(entries, Entry{Time: yesterday, Duration: 150})

	time, duration := entries.ToDurationSeries()

	assert.Equal(t, yesterday, time[0])
	assert.Equal(t, today, time[1])
	assert.Equal(t, float64(150), duration[0])
	assert.Equal(t, float64(100), duration[1])
}
