package rails

import (
	"sort"
	"time"
)

type Entry struct {
	Method         string    // "GET"
	Path           string    // "/autocomplete/users.json"
	Format         string    // "json"
	Controller     string    // "AutocompleteController" TODO ignored
	Action         string    // "users"
	Status         int       // 200
	Duration       float64   // 55.93,
	View           float64   // 2.43
	Db             float64   // 10.16
	Time           time.Time // "2019-04-29T05:44:56.529Z"
	params         []string  // [{"key":"search","value":"val"},{"key":"active","value":"true"}]
	Remote_ip      string    // "10.117.114.34"
	User_id        int       // 123
	Username       string    // "someone"
	ua             string    // "Mozilla/5.0 (Windows NT 10.0; WOW64) ... TODO ignored
	Queue_duration float64   // 8.15
	Correlation_id string    // "qLnXAehIB74"
}

func (entry Entry) Matches(filter Filter) bool {
	return entry.Controller == filter.controller &&
		entry.Action == filter.action &&
		entry.Format == filter.format
}

type Entries []Entry

func (e Entries) Len() int {
	return len(e)
}

func (e Entries) Less(i, j int) bool {
	return e[i].Time.Before(e[j].Time)
}

func (e Entries) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

func (e Entries) ToDurationSeries() ([]time.Time, []float64) {
	time := make([]time.Time, len(e))
	duration := make([]float64, len(e))

	sort.Sort(e)

	for index, entry := range e {
		time[index] = entry.Time
		duration[index] = entry.Duration
	}

	return time, duration
}
