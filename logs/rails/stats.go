package rails

import (
	"fmt"
	"io"
	"math"
	"os"
	"time"
)

type Stats struct {
	entries      Entries
	slowest      Entries
	size         int
	min_duration float64
	avg_duration float64
	max_duration float64
}

func NewStats(entries Entries) *Stats {
	stats := &Stats{
		entries:      entries,
		size:         len(entries),
		min_duration: math.Inf(1),
		max_duration: math.Inf(-1),
	}

	return stats.Process()
}

func (stats *Stats) Process() *Stats {
	if stats.size == 0 {
		panic("no logs selected")
	}

	for _, entry := range stats.entries {
		if entry.Duration < stats.min_duration {
			stats.min_duration = entry.Duration
		}

		if entry.Duration > stats.max_duration {
			stats.max_duration = entry.Duration
		}

		stats.avg_duration = stats.avg_duration + entry.Duration
	}

	stats.avg_duration = stats.avg_duration / float64(stats.size)

	return stats
}

func (stats *Stats) ToDurationSeries() ([]time.Time, []float64) {
	return stats.entries.ToDurationSeries()
}

func (stats *Stats) Print(writers ...io.Writer) *Stats {
	if len(writers) == 0 {
		writers = append(writers, os.Stdout)
	}

	for _, output := range writers {
		fmt.Fprintf(output, "Log entries: %d, avg: %.2f, min: %.2f, max: %.2f [ms]\n",
			stats.size,
			stats.avg_duration,
			stats.min_duration,
			stats.max_duration)
	}

	return stats
}
