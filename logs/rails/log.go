package rails

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"sync"
)

type Log struct {
	path   string
	filter Filter
}

type Endpoint struct {
	Path string
}

type Filter struct {
	controller string
	action     string
	format     string
}

func Load(path, selector string) *Stats {
	endpoint := Endpoint{Path: selector}

	log := Log{
		path:   path,
		filter: endpoint.ToFilter(),
	}

	return log.Load()
}

func (endpoint Endpoint) ToFilter() Filter {
	components := strings.Split(endpoint.Path, "#")
	controller := components[0]

	components = strings.Split(components[1], ".")
	action := components[0]
	format := components[1]

	return Filter{
		controller: controller,
		action:     action,
		format:     format,
	}
}

func (log Log) Load() *Stats {
	var entries Entries
	var wg sync.WaitGroup

	file, err := os.Open(log.path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	echan := make(chan Entry)

	go func() {
		for entry := range echan {
			entries = append(entries, entry)
		}
	}()

	for scanner.Scan() {
		wg.Add(1)
		go log.unmarshal(scanner.Text(), &wg, echan)
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	} else {
		wg.Wait()
		close(echan)
	}

	return NewStats(entries)
}

func (log Log) unmarshal(str string, wg *sync.WaitGroup, entries chan<- Entry) {
	var entry Entry
	defer wg.Done()

	err := json.Unmarshal([]byte(str), &entry)
	if err != nil {
		fmt.Println("error:", err)
		panic("could not unmarshal an entry")
	}

	if entry.Matches(log.filter) {
		entries <- entry
	}
}
