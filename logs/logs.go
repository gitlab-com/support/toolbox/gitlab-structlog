package logs

import (
	"gitlab.com/gitlab-com/support/toolbox/gitlab-structlog/logs/rails"
)

func LoadRails(path, filter string) *rails.Stats {
	return rails.Load(path, filter)
}
