module gitlab.com/gitlab-com/support/toolbox/gitlab-structlog

go 1.12

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/afero v1.2.1
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.20.0
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	golang.org/x/image v0.0.0-20190501045829-6d32002ffd75 // indirect
)
