# gitlab-structlog

## Why

To make sure that we optimize the right thing.

`gitlab-structlog` can analyze a gigabytes of structured logs in a short period
of time. Currently at around 100 megabytes per second.

It provides some stats and generates a graph like the one below.

![duration timeseries](https://gitlab.com/gitlab-org/gitlab-ce/uploads/3706084605a39907eefebc41456bc91b/output.png)

## Usage

```bash
$ go get gitlab.com/gitlab-com/support/toolbox/gitlab-structlog
$ gitlab-structlog analyze --help
$ gitlab-structlog analyze                          \
  --controller "Projects::JobsController#show.json" \
  --file gitlab-rails/production_json.log           \
  --graph output.png
```
